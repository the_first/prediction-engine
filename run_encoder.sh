#!/usr/bin/env bash

# eval $(minikube docker-env)

docker build -t eu.gcr.io/xcc-reverse-image-wed/encoder:latest encoder
docker push eu.gcr.io/xcc-reverse-image-wed/encoder:latest
kubectl apply -f encoder/encoder_deployment.yaml
kubectl apply -f encoder/encoder_service.yaml

# eval $(minikube docker-env -u)



#docker build -t selinencoder encoder
#kubectl apply -f encoder/encoder_deployment.yaml

 #kubectl port-forward encoder-787648fb68-t77m8 5000
