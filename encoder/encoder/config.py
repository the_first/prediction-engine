import yaml
import logging

import os

ROOT_DIR = os.path.dirname(os.path.realpath(__file__))


def get_config(config_section: str):
    logging.info("Reading the config file.. " + ROOT_DIR + "/config.yaml")
    config_yaml = ROOT_DIR + "/config.yaml"
    with open(config_yaml, 'r') as ymlfile:
        config = yaml.load(ymlfile)

    result = config[config_section]
    return result


def get_project_id():
    return get_config("project_id")


def get_credentials():
    return get_config("credentials")


def get_bucket_name():
    return get_config("bucket_name")


def get_train_images_dir():
    return get_config("train_images_dir")


def get_bigquery_name():
    return get_config("bigquery_name")
