import argparse
import os
import PIL.Image
import numpy as np
import torch
import requests

from conn.gcs import yield_public_url_blobs, insert_features_bigquery
from torch import nn
from torch.utils.data import Dataset
from torchvision import models
from torchvision import transforms
from torchvision.datasets import ImageFolder
from torchvision.models.resnet import model_urls
from tqdm import tqdm
from PIL import Image
from io import BytesIO

from config import get_bucket_name, get_train_images_dir

np.random.seed(0)
torch.manual_seed(0)


def str_is_url(url):
    from urllib.parse import urlparse

    try:
        result = urlparse(url)
        return all([result.scheme, result.netloc])
    except ValueError:
        return False


class Encoder:
    """Abstract class for encoding an image"""

    def __call__(self, image):
        raise NotImplementedError


class PixelEncoder(Encoder):
    def __init__(self, x=0, y=0):
        """Encodes an image by taking the pixel value (x, y) from an image."""
        self.x = x
        self.y = y

    def __call__(self, image: PIL.Image):
        """Extract the red channel pixel value from the location (x, y) in the image"""
        r, _, _ = image.getpixel((self.x, self.y))
        return r


class SingleImage(Dataset):
    """Class for reading and transforming a single image
    """

    def __init__(self, image_path: str, transformations):
        self.image_path = image_path
        self.is_url = str_is_url(image_path)
        self.transformations = transformations
        self.imgs = [image_path]

    def __len__(self):
        return 1

    def __getitem__(self, item):
        if self.is_url:
            response = requests.get(self.image_path)
            img = Image.open(BytesIO(response.content))
        else:
            img = Image.open(self.image_path)
        return self.transformations(img)


class FeatureEncoder(Encoder):
    """Class for extracting features from a single image or a directory of subdirectories with images
    """

    def __init__(self, batch_size: int):
        self.transformations = self.initialize_transformations()
        self.batch_size = batch_size
        self.model = self.initialize_model()

    def __call__(self, data_path: str):
        """Extract features from the images in the provided data_path
        :param data_path: points to a single image or a directory of subdirectories which contain images
        :return: numpy array of the features
        """
        data = self.create_dataset(data_path, self.transformations)
        batch_size = 1 if len(data) == 1 else self.batch_size
        dataloader = self.create_dataloader(data, batch_size=batch_size)

        self.model.eval()
        out_features = torch.empty((len(data), 2048))
        with torch.no_grad():
            for idx, batch in enumerate(tqdm(dataloader)):
                to_predict = (
                    batch[0].view(1, 3, 226, 226)
                    if len(batch[0].shape) == 3
                    else batch[0]
                )
                pred = self.model(to_predict)
                start_out_idx = idx * dataloader.batch_size
                out_features[
                    start_out_idx : start_out_idx + dataloader.batch_size, :
                ] = pred

        all_img_paths = np.array([i[0] if isinstance(i, tuple) else i for i in data.imgs])
        img_path_feature_array_dict = {
            img_path: arr for img_path, arr in zip(all_img_paths, out_features.numpy())
        }
        return img_path_feature_array_dict

    @staticmethod
    def create_dataset(data_path, transformations):
        """
        Create a dataset for the directory or the single image
        :param data_path: points to a single image or a directory of subdirectories which contain images
        :param transformations: transformation to apply to the image(s)
        :return:
        """
        if os.path.isfile(data_path) or str_is_url(data_path):
            return SingleImage(data_path, transformations)

        return ImageFolder(data_path, transformations)

    @staticmethod
    def create_dataloader(data, batch_size=32, num_workers=0, pin_memory=False):
        """
        Create a dataload object
        :param data: torch dataset
        :param batch_size: batch size
        :param num_workers: number of workers
        :param pin_memory: setting to write to cuda memory
        :return:
        """
        return torch.utils.data.DataLoader(
            data,
            batch_size=batch_size,
            shuffle=False,
            num_workers=num_workers,
            pin_memory=pin_memory,
        )

    @staticmethod
    def initialize_transformations():
        """
        Initialize the image transformations
        """
        return transforms.Compose(
            [
                transforms.Resize(256),
                transforms.CenterCrop(226),
                transforms.ToTensor(),
                transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225]),
            ]
        )

    @staticmethod
    def initialize_model():
        """
        Initialize the resnet model
        """
        # Prevent ssl certification error on Mac OS
        model_urls['resnet50'] = model_urls['resnet50'].replace('https://', 'http://')
        model_ft = models.resnet50(pretrained=True)
        # Replace the last layer with something empty to extract the features
        model_ft.fc = nn.Sequential()
        return model_ft


def encode_all_the_images_from_blob(class_folder):
    # parser = argparse.ArgumentParser(
    #     description='Extract features for image'
    # )
    # parser.add_argument(
    #     '--class-folder',
    #     required=True,
    #     type=str,
    #     help='Subfolder in root directory containing images of a single class'
    # )
    # args, _ = parser.parse_known_args()

    feature_extractor = FeatureEncoder(batch_size=32)
    bucket_name = get_bucket_name()
    # class_folder = args.class_folder

    print("Getting blobs, this takes a while...")
    for path in yield_public_url_blobs(
        bucket_name, get_train_images_dir() + class_folder
    ):
        features = feature_extractor(path)
        insert_features_bigquery(features)
    return True


# if __name__ == '__main__':
#     try:
#         main()
#     except KeyboardInterrupt:
#         print("You stopped the training program.")
