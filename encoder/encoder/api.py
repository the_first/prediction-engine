from encoders import PixelEncoder, encode_all_the_images_from_blob

from flask import Flask, request
import os


app = Flask(__name__)


@app.route('/feature_encoder', methods=['POST'])
def get_feature_encoder():
    if request.method == 'POST':
        # g.data contains the payload
        content = request.get_json() or request.values
        pixel_encoded_results = PixelEncoder(content)
        return pixel_encoded_results


# encode_all_images end point should be deleted and created as a batch job
@app.route('/encode_all_images', methods=['GET'])
def get_encode_all_the_images_from_blob():
    if request.method == 'GET':
        class_folder = request.args.get('class_folder')
        if class_folder:
            encode_all_the_images_from_blob(class_folder)
        else:
            return "Please pass class folder"


if __name__ == '__main__':
    os.environ["FLASK_DEBUG"] = "1"
    app.run(debug=True, host='0.0.0.0', port=5000)
