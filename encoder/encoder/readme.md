# How to extract features from an image (directory)

    python resnet_encoder.py \
        --data_path=<path_to_dir_or_file> \
        --output_path=<path_to_save_array>

## Provide a directory as data_path

Make sure it has the following structure:
    
    - your_path
        - class_1
            - _.jpg
            - ..
        - class_2
            - _.jpg
            - ..
        - ..
            - ..

Providing a path with only one folder is fine
