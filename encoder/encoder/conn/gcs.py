import os

from dotenv import load_dotenv
from google.cloud import storage, bigquery
from google.auth import compute_engine, exceptions
from config import get_project_id, get_credentials, get_bigquery_name

# following 2 lines might change with dotenv

project_id = get_project_id()
credentials = get_credentials()
bigquery_name = get_bigquery_name()

def get_client(lib, project_id=project_id):
    """
    Get client of given lib in cloud or locally.
    """
    try:
        return getattr(lib, 'Client')()
    except exceptions.DefaultCredentialsError:
        return getattr(lib, 'Client')(compute_engine.Credentials(), project_id)


def insert_features_bigquery(features, client=get_client(bigquery)):
    for url, values in features.items():
        query = (
            f"INSERT INTO `{bigquery_name}` "
            f"VALUES ('{url}', {', '.join([str(x) for x in values])})"
        )
        query_job = client.query(query, location="US",)
        for row in query_job:
            print('.', end='')


def upload_blob(
    bucket_name, source_file_name, destination_blob_name, client=get_client(storage)
):
    """
    Uploads a file to the bucket.
    """
    bucket = client.get_bucket(bucket_name)
    blob = bucket.blob(destination_blob_name)

    blob.upload_from_filename(source_file_name)

    print('File {} uploaded to {}.'.format(source_file_name, destination_blob_name))

    return destination_blob_name


def download_blob(
    bucket_name, source_blob_name, destination_file_name, client=get_client(storage)
):
    """
    Downloads a blob from the bucket.
    """
    bucket = client.get_bucket(bucket_name)
    blob = bucket.blob(source_blob_name)

    blob.download_to_filename(destination_file_name)

    print('Blob {} downloaded to {}.'.format(source_blob_name, destination_file_name))


def get_filename_from_blob(blob):
    return repr(blob).split(',')[1].strip()


def yield_public_url_blobs(bucket_name, folder='', client=get_client(storage)):
    """
    Lists all the blobs in the bucket.
    blob is a string with a lot of information including the URL.
    """
    for blob in client.list_blobs(bucket_name):

        filename = get_filename_from_blob(blob)
        if filename.startswith():
            yield os.path.join(
                'https://storage.googleapis.com',folder
                'eu.artifacts.xcc-reverse-image-wed.appspot.com',
                filename,
            )
