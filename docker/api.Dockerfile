FROM pytorch/pytorch:latest

WORKDIR /app

ENV LC_ALL C.UTF-8
ENV LANG C.UTF-8

COPY Pipfile /app
COPY Pipfile.lock /app

RUN pip install -U setuptools pip
RUN pip install pipenv
RUN apt-get -y update && apt-get -y install unixodbc-dev
RUN pipenv install --system --deploy
COPY dist/prediction_engine* /app

RUN pip install ./*.whl
RUN rm /app/*.whl

CMD gunicorn --worker-tmp-dir /dev/shm \
    --workers=2 --threads=4 \
    --worker-class=gthread \
    --bind 0.0.0.0:80 prediction_engine.api

