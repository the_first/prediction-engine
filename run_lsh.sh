#!/usr/bin/env bash

# re-use the Docker daemon inside the Minikube instance
# eval $(minikube docker-env)

docker build -t eu.gcr.io/xcc-reverse-image-wed/lsh:latest lsh
kubectl apply -f lsh/lsh_job.yaml

# eval $(minikube docker-env -u)

docker push eu.gcr.io/xcc-reverse-image-wed/lsh:latest
