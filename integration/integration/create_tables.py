import os

from google.cloud import bigquery


class DataBuild:

    def __init__(self):
        self.project_name = "xcc-reverse-image-wed"
        self.dataset_name = "image_sets"
        self.client = self.get_client()
        self.google_credentials = os.getenv('GOOGLE_APPLICATION_CREDENTIALS')

    def get_client(self):
        # project_name = os.environ['PROJECT_NAME']
        return bigquery.Client(project=self.project_name)

    def create_predict_fit(self):
        table_names = ["predicted_images", "trained_images"]
        for table_name in table_names:
            schema = [
                bigquery.SchemaField("uuid", "INTEGER", mode="REQUIRED"),
                bigquery.SchemaField("array", "STRING", mode="REQUIRED"),
                bigquery.SchemaField("image_url", "STRING", mode="REQUIRED"),
            ]

            # TODO(developer): Set table_id to the ID of the table to create
            table_id = '.'.join([
                self.project_name,
                self.dataset_name,
                table_name
            ])

            table = bigquery.Table(table_id, schema=schema)
            table = self.client.create_table(table)  # API request
            print("Created table {}.{}.{}".format(
                table.project, table.dataset_id, table.table_id))

    def create_data_set(self):
        dataset_id = "{}.image_sets".format(self.client.project)
        dataset = bigquery.Dataset(dataset_id)
        dataset.location = "EU"
        dataset = self.client.create_dataset(dataset)  # API request
        print("Created dataset {}.{}".format(
            self.client.project, dataset.dataset_id))
        print("Woohoooo!!!!!")
        return dataset

    def create_all_data_storage(self):
        self.create_data_set()
        self.create_predict_fit()


if __name__ == "__main__":
    try:
        data = DataBuild()
        data.create_all_data_storage()
    except KeyboardInterrupt:
        print("You stopped the program.")
