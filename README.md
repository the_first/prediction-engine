# prediction-engine

## 1. Intro

This project consists of:
- Image encoder docker container
- LSH docker container
- Website that accepts images and displays similar images

Engineering perspective:
![](res/engineering-perspective.jpg)

Science perspective:
![](res/science-perspective.jpg)

## 2. Getting Started


```
  {
    "url": "data\/input\/Abyssinian_1.jpg",
    "encoding": 30,
    "distance": 0
  },
  {
    "url": "data\/input\/Abyssinian_3.jpg",
    "encoding": 28,
    "distance": 2
  },
  ...
  {
    "url": "data\/input\/Abyssinian_7.jpg",
    "encoding": 241,
    "distance": 211
  }
]
```



## Credits
This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage



## Running locally
minikube start --memory 8192 --cpus 4 --disk-size 100g




# Running in cloud
gcloud auth login

gcloud config set project xcc-reverse-image-wed

`bash run_encoder.sh`

go to http://[IP_Address_of_services]:[port_of_services]/encode_all_images?class_folder=n01440764
