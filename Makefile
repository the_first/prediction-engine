.PHONY: clean clean-test clean-pyc clean-build docs help
.DEFAULT_GOAL := help

define BROWSER_PYSCRIPT
import os, webbrowser, sys

try:
	from urllib import pathname2url
except:
	from urllib.request import pathname2url

webbrowser.open("file://" + pathname2url(os.path.abspath(sys.argv[1])))
endef
export BROWSER_PYSCRIPT

define PRINT_HELP_PYSCRIPT
import re, sys

for line in sys.stdin:
	match = re.match(r'^([a-zA-Z_-]+):.*?## (.*)$$', line)
	if match:
		target, help = match.groups()
		print("%-20s %s" % (target, help))
endef
export PRINT_HELP_PYSCRIPT

BROWSER := python -c "$$BROWSER_PYSCRIPT"

app: ## run the flask app locally
	FLASK_APP=prediction_engine/api.py ENCODING_PATH="data/encoding/img_encoding.csv" flask run

help:
	@python -c "$$PRINT_HELP_PYSCRIPT" < $(MAKEFILE_LIST)

clean: clean-build clean-pyc clean-test ## remove all build, test, coverage and Python artifacts

clean-build: ## remove build artifacts
	rm -fr build/
	rm -fr dist/
	rm -fr .eggs/
	find . -name '*.egg-info' -exec rm -fr {} +
	find . -name '*.egg' -exec rm -f {} +

clean-pyc: ## remove Python file artifacts
	find . -name '*.pyc' -exec rm -f {} +
	find . -name '*.pyo' -exec rm -f {} +
	find . -name '*~' -exec rm -f {} +
	find . -name '__pycache__' -exec rm -fr {} +

clean-test: ## remove test and coverage artifacts
	rm -fr .tox/
	rm -f .coverage
	rm -fr htmlcov/
	rm -fr .pytest_cache

lint: ## check style with flake8
	flake8 prediction_engine tests

test: ## run tests quickly with the default Python
	py.test

test-all: ## run tests on every Python version with tox
	tox

coverage: ## check code coverage quickly with the default Python
	coverage run --source prediction_engine -m pytest
	coverage report -m
	coverage html
	$(BROWSER) htmlcov/index.html

docs: ## generate Sphinx HTML documentation, including API docs
	rm -f docs/prediction_engine.rst
	rm -f docs/modules.rst
	sphinx-apidoc -o docs/ prediction_engine
	$(MAKE) -C docs clean
	$(MAKE) -C docs html
	$(BROWSER) docs/_build/html/index.html

servedocs: docs ## compile the docs watching for changes
	watchmedo shell-command -p '*.rst' -c '$(MAKE) -C docs html' -R -D .

release: dist ## package and upload a release
	twine upload dist/*

dist: clean ## builds source and wheel package
	python setup.py sdist
	python setup.py bdist_wheel
	ls -l dist

install: clean ## install the package to the active Python's site-packages
	python setup.py install

build:
	pip install wheel
	python setup.py bdist_wheel

# 1) https://cloud.google.com/cloud-build/docs/quickstart-docker
# 2) https://cloud.google.com/run/docs/deploying

# 1. The encoder takes images from blob storage, and writes their
# features to BigQuery.

dockerize-encoder: build
	docker build --rm -f "docker/encoder/encoder.Dockerfile" -t eu.gcr.io/xcc-reverse-image-wed/encoder:latest .

publish-encoder:
	@echo See https://console.cloud.google.com/gcr/images/xcc-reverse-image-wed?project=xcc-reverse-image-wed
	docker push eu.gcr.io/xcc-reverse-image-wed/encoder:latest

run-encoder-cloud:
	@echo See https://console.cloud.google.com/run?project=xcc-reverse-image-wed
	kubectl apply -f docker/encoder/encoder_job.yaml

run-encoder-locally:
	@echo "Running docker container"
	docker run --rm -it \
	--env-file='.env' \
	eu.gcr.io/xcc-reverse-image-wed/encoder:latest python -m prediction_engine.encoders --class-folder n01440764

# 2. The LSH'er takes image features and outputs cluster data back
# to a different BigQuery table.

dockerize-lsh: build
	docker build --rm -f "docker/lsh/lsh.Dockerfile" -t eu.gcr.io/xcc-reverse-image-wed/lsh:latest .

publish-lsh:
	@echo See https://console.cloud.google.com/gcr/images/xcc-reverse-image-wed?project=xcc-reverse-image-wedproject=xcc-reverse-image-wed
	docker push eu.gcr.io/xcc-reverse-image-wed/lsh:latest

run-lsh-cloud:
	@echo See https://console.cloud.google.com/run?project=xcc-reverse-image-wed
	gcloud beta run deploy test123 --image eu.gcr.io/xcc-reverse-image-wed/lsh:latest --platform managed

run-lsh-locally:
	@echo "Running docker container"
	docker run --rm -it \
	-v $$(pwd):/app \
	--env-file='.env' \
	eu.gcr.io/xcc-reverse-image-wed/lsh:latest python -m prediction_engine.locality_sensitive_hasher


# 3. The API includes a single landing page that allows a user to upload
# an image. The api will query BigQuery

dockerize-api: build
	docker build --rm -f "docker/api.Dockerfile" -t eu.gcr.io/xcc-reverse-image-wed/api:latest .

publish-api:
	@echo See https://console.cloud.google.com/gcr/images/xcc-reverse-image-wed?project=xcc-reverse-image-wedproject=xcc-reverse-image-wed
	docker push eu.gcr.io/xcc-reverse-image-wed/api:latest

run-api-cloud:
	@echo See https://console.cloud.google.com/run?project=xcc-reverse-image-wed
	gcloud beta run deploy test123 --image eu.gcr.io/xcc-reverse-image-wed/api:latest --platform managed

run-api-locally:
	@echo "Running docker container"
	docker run --rm -it \
	-p 8080:8080 \
	-v $$(pwd):/app \
	--env-file='.env' \
	eu.gcr.io/xcc-reverse-image-wed/api:latest python -m prediction_engine.api


