# -*- coding: utf-8 -*-

"""Top-level package for prediction-engine."""

__author__ = """Xccelerated"""
__email__ = 'svandorsten@xccelerated.io'
__version__ = '0.1.0'
