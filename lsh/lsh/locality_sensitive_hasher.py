# -*- coding: utf-8 -*-

"""Main module."""
import pickle
import numpy as np
import logging
import pandas as pd

np.random.seed(0)


class LocalSensitiveHasher:
    """
    Implementation of LSH
    (https://towardsdatascience.com/understanding-locality-sensitive-hashing-49f6d1f6134)
    """

    def __init__(self, n_features: int, n_hashes: int) -> None:
        self._logger = logging.getLogger(__name__)
        self.n_features = n_features
        self.n_hashes = n_hashes
        self.random_matrix = self.init_rand_matrix()
        self._logger.info('Init completed')

    def init_rand_matrix(self) -> np.array:
        """
        Create matrix with random numbers between -1 and 1 of shape
        n_features x n_hashes
        Initialize once and always use this matrix to hash new images
        """
        self._logger.info('Initializing hashing matrix')
        return np.random.uniform(low=-1,
                                 high=1,
                                 size=(self.n_hashes,
                                      self.n_features)
                                 )
    def calculate_proj_matrix(self, dataset: np.array) -> np.array:
        """
        Projects dataset to lower dimensional hashing space.

        Let:
        - n denote number of image encodings (number of images), 
        - d denote the number of features 
        - k denote the number of hashes 
        
        Then the expected shape of:
        - Hashing matrix (R - random matrix) is k x d (a row per hash function, a column per feature)
        - Dataset (D) is n x d (a row per image, a column per feature) 
        - Resulting matrix (P -projection matrix) is k x n (a row per hashed feature, a column per image)
        
        P = R x D_transpose
        (k x n) = (k x d) x (d x n)
        """

        # TODO (ENGINEERING): Make calc of inproduct scalable to ~2e3 x 7e6 matrix
        self._logger.info('Calculating projection')
        return np.dot(self.random_matrix, dataset.T)
    
    def projection_to_binary(self, projection: np.array) -> np.array:
        """
        Encodes projection matrix values to binary values.
        The returned matrix should have the same shape as the projection matrix: k x n.
        """
        self._logger.info('Transforming to binary')
        projection[projection >= 0] = 1
        projection[projection < 0] = 0
        binary = projection.astype(int)
        self._logger.info('Binary transformation completed')
        return binary
    
    def binary_to_buckets(self, binary: np.array) -> np.array:
        """
        Label binary buckets as decimal values
        :param binary: a binary matrix 
        :return: a bucket name for each ordering of 0s and 1s in the columns of the binary matrix
        
        The returned matrix should be of shape 1 x n (a bucket for each image)
        """
        self._logger.info('Transforming binary matrix to buckets')
        return [int("".join([x for x in binary[:, col_id].astype(str)]), 2)
                for col_id in range(binary.shape[1])]
    
    
    def calculate_bucket_dict(self, bucket_list: list) -> dict:
        """
        Transform a list of bucket_ids, with the list index corresponding to image_ids, to a
        dictionary of the form {bucket_id: [image_id1, image_id2, ..., imageidn]}.
        """

        self._logger.info('Transforming bucket list to dictionary')
        return {key: [n for n, x in enumerate(bucket_list) if x == key]
                for key in set(bucket_list)}
    
    def __call__(self, dataset: np.array) -> list:
        """
        Hash full dataset to bucket numbers. Dataset is expected to be a 2D array in where each
        row represents a single image signature and each column a signature feature.

        A list of bucket ids is returned, in where the list index of the bucket id corresponds
        to the accompanying image id.
        """

        projection = self.calculate_proj_matrix(dataset)
        binary_projection = self.projection_to_binary(projection)
        bucket_projection = self.binary_to_buckets(binary_projection)

        if len(bucket_projection) == 1:
            return bucket_projection[0]
        else:
            return self.calculate_bucket_dict(bucket_projection)
        
        
    def save_self(self, filepath: str) -> None:
        """
        Save class instance to file. Required to persist the hash random matrix
        """

        with open(filepath, 'wb') as outfile:
            pickle.dump(self, outfile)



if __name__ == '__main__':
    # The following path should be changed to the one of the BQ table
    # The BQ table should have the following schema
    # | index | url | f0 | f1 | f2 | ..
    images = pd.read_csv('data/encoding/img_encoding.csv')
    # Dropping the index and the url to retain only the encoded features
    encoding_dataset = images.drop(['index', 'url'], axis=1)
    n_features = encoding_dataset.shape[1]  # Make sure the dataset is of shape n x d
    hasher = LocalSensitiveHasher(n_features=n_features,
                                  n_hashes=10)

    # for every image, calculate the hash bucket.
    list_of_buckets = hasher(dataset=encoding_dataset)

    # Transform list of buckets to a dictionary that can be stored in a database
    # bucket_dict = hasher.calculate_bucket_dict(bucket_list=list_of_buckets)

    # Serialize hasher to persist for later use. NOTE: the hashing is unique: the exact same hasher
    # is required for the stored bucket_dict for making comparisons!
    store_hasher_location = '../hasher.pkl'
    hasher.save_self(filepath=store_hasher_location)

    print(list_of_buckets)
