from PIL import Image
import pandas as pd
from typing import Optional

from prediction_engine.encoders import Encoder, PixelEncoder, FeatureEncoder
from prediction_engine.locality_sensitive_hasher import LocalSensitiveHasher


class Model:
    def __call__(self, image, *args, **kwargs):
        raise NotImplementedError

    def predict(self, image, *args, **kwargs):
        """Wrapper method to be compliant with standard prediction APIs."""
        return self.__call__(image, *args, **kwargs)


class DummyModel(Model):
    def __init__(self, encodings: pd.DataFrame, encoder: Encoder = PixelEncoder()):
        """Dummy model used for predicting similar images

        :param encodings: pd.DataFrame object containing a column with
        :param encoder: Encoder class
        """
        self.encoder = encoder

        if 'encoding' not in encodings.columns:
            raise ValueError(f"Missing column in encodings DataFrame: 'encoding'.")
        self.encodings = encodings

    def __call__(self, image: Image, k: Optional[int] = None, *args, **kwargs):
        """Encode an input image and return the most similar k images

        :param image: (PIL.Image) image to predict
        :param k: number of images to return
        :return: a sorted DataFrame with the most similar images
        """
        encoded_image = self.encoder(image)

        return (self.encodings
                .assign(distance=lambda d: abs(d.encoding - encoded_image))
                .sort_values('distance')
                .head(k)
                )


class LSHModel(Model):
    def __init__(self,
                 encoder: Encoder,
                 hasher: LocalSensitiveHasher,
                 ) -> None:
        """
        !!! IMPORTANT !!! Load the exact same LocalSensitiveHasher instance as used for creating the
        bucket_id_dict. The hash functions have a random generated component that needs to be the
        same during hashing ("fitting") and finding similar images ("prediction").

        :param hasher: Hasher instance used to create bucket_id_dict
        :param encoder: Encoder used to create encodings when creating bucket_id_dict
        """
        self.encoder = encoder
        self.hasher = hasher

    def __call__(self,
                 image: Image,
                 *args,
                 **kwargs
                 ) -> int:
        """
        Take an image and give back hash bucket ID.

        Other images with the same hash bucket ID are similar to the given image.

        :param image: Image to get bucket ID for
        :param args: Not used
        :param kwargs: Not used
        :return: bucket id
        """

        encoded_image = self.encoder(image)
        return self.hasher(encoded_image)
