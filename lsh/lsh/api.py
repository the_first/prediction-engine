from flask import Flask, request, render_template, abort
from prediction_engine.models import LSHModel, DummyModel
from tempfile import NamedTemporaryFile
from prediction_engine.conn import gcs
from dotenv import load_dotenv

import pandas as pd
import PIL.Image
import os

app = Flask(
    __name__,
    static_folder="./view/static",
    template_folder="./view"
)

app.config['UPLOAD_FOLDER'] = 'data/images'
ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg'}

load_dotenv('.env')
encoding_path = os.getenv('ENCODING_PATH')

# #TODO: Import from encoders instead of reading from csv
encodings = pd.read_csv(encoding_path, index_col=0)


@app.route('/', methods=['GET'])
def index():
    return render_template('index.html')


@app.route('/', methods=['POST'])
def predict():
    """Get the most similar images from our dataset"""

    filename = 'image'
    if filename not in request.files:
        return abort(400, "No file provided")

    # Get the image from the request
    image = request.files[filename]

    if image.mimetype.split('/')[-1] not in ALLOWED_EXTENSIONS:
        return abort(400, "That filetype is not allowed!")

    # Make prediction
    model = DummyModel(encodings)
    img = PIL.Image.open(image).convert('RGB')
    predictions = model(img)

    # Return similar images
    return render_template(
        'similar_images.html',
        json_prediction=predictions.to_dict().get('url')
    )


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8080)
