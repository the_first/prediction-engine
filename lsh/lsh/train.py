from fire import Fire
import os
import pandas as pd
import PIL.Image
from typing import List, Optional

from prediction_engine.encoders import Encoder, PixelEncoder


def generate_encodings(url: List[str], encoder: Encoder, encoding_path: str):
    """Fake Training script to generate encodings for testing"""

    result = {}

    for url in urls:
        img = PIL.Image.open(url)
        img = img.convert('RGB')
        encoding = encoder(img)
        result.update({url: encoding})

    (pd.DataFrame.from_dict(result, orient='index')
     .reset_index()
     .rename(columns={
        'index': 'url',
        0: 'encoding'}
    )
     .to_csv(encoding_path)
     )


if __name__ == '__main__':
    url_path = 'data/input'
    urls = [os.path.join(url_path, filename) for filename in os.listdir(url_path) if filename.endswith('jpg')]

    encoding_path = 'data/encoding/img_encoding.csv'
    generate_encodings(
        urls,
        encoder=PixelEncoder(),
        encoding_path=encoding_path
    )
