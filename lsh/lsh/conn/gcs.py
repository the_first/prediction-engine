import os

from dotenv import load_dotenv
from google.cloud import storage, bigquery
from google.auth import compute_engine, exceptions

load_dotenv('.env')


def get_client(lib, project_id=os.getenv('PROJECT_ID')):
    """
    Get client of given lib in cloud or locally.
    """
    try:
        return getattr(lib, 'Client')()
    except exceptions.DefaultCredentialsError:
        return getattr(lib, 'Client')(compute_engine.Credentials(), project_id)


def insert_features_bigquery(features, client=get_client(bigquery)):
    for url, values in features.items():
        query = (
            "INSERT INTO `xcc-reverse-image-wed.features.v1` "
            f"VALUES ('{url}', {', '.join([str(x) for x in values])})"
        )
        query_job = client.query(query, location="US",)
        for row in query_job:
            print('.', end='')


def read_features_bigquery(client=get_client(bigquery)):
    """Read the features that encoder produce from Google big-query

    Parameters
    ----------
    client : Object, optional
             by default google.cloud.bigquery.client.Client object

    Output
    ---------
    tupple : (the url of the image, the features in 2048 float inside a tuple)
    """
    query = "SELECT * FROM `xcc-reverse-image-wed.features.v1` "
    query_job = client.query(query, location="US",)
    for row in query_job:
        yield row[0], row[1:]


def upload_blob(
    bucket_name, source_file_name, destination_blob_name, client=get_client(storage)
):
    """
    Uploads a file to the bucket.
    """
    bucket = client.get_bucket(bucket_name)
    blob = bucket.blob(destination_blob_name)

    blob.upload_from_filename(source_file_name)

    print('File {} uploaded to {}.'.format(source_file_name, destination_blob_name))

    return destination_blob_name


def download_blob(
    bucket_name, source_blob_name, destination_file_name, client=get_client(storage)
):
    """
    Downloads a blob from the bucket.
    """
    bucket = client.get_bucket(bucket_name)
    blob = bucket.blob(source_blob_name)

    blob.download_to_filename(destination_file_name)

    print('Blob {} downloaded to {}.'.format(source_blob_name, destination_file_name))


def yield_public_url_blobs(bucket_name, folder='', client=get_client(storage)):
    """
    Lists all the blobs in the bucket.
    """
    for blob in client.list_blobs(bucket_name):

        path = repr(blob).split(',')[1].strip()
        if path.startswith(folder):
            yield os.path.join(
                'https://storage.googleapis.com',
                'eu.artifacts.xcc-reverse-image-wed.appspot.com',
                path,
            )
