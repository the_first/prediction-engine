#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""The setup script."""

from setuptools import setup, find_packages
import json
import os


def open_file(fname):
    return open(os.path.join(os.path.dirname(__file__), fname))


def read_pipenv_dependencies(fname):
    lockfile = open_file(fname)
    lockjson = json.load(lockfile)
    return [dependency for dependency in lockjson.get('default')]


setup(
    author="Xccelerated",
    author_email='svandorsten@xccelerated.io',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        "Programming Language :: Python :: 2",
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
    ],
    description="lsh",
    license="MIT license",
    include_package_data=True,
    install_requires=[
        *read_pipenv_dependencies('../Pipfile.lock'),
    ],
    keywords='lsh',
    name='lsh',
    packages=find_packages(include=['encoder*']),
    url='https://github.com/--/prediction_engine',
    version='0.1.0',
    zip_safe=False,
)
