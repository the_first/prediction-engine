#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Tests for `prediction_engine` package."""

import pytest


from prediction_engine import prediction_engine
from prediction_engine import init_rand_matrix
from prediction_engine import calculate_proj_matrix
import numpy as np


def test_init_rand_matrix(test_shapes):
    n_features = test_shapes[0]
    n_hashes = test_shapes[1]

    assert init_rand_matrix(n_features, n_hashes).shape == (n_features, n_hashes)
    assert init_rand_matrix(n_features, n_hashes).min() >= -1
    assert init_rand_matrix(n_features, n_hashes).max() <= 1


def test_calculate_proj_matrix():
    n_features = 10
    n_datapoints = 100
    n_hashes = 4

    random_matrix = init_rand_matrix(n_features, n_hashes)

    dataset = np.random.rand(n_features,n_datapoints)

    assert calculate_proj_matrix(random_matrix, dataset).shape == (n_hashes, n_datapoints)
